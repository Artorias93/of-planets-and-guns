// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OfPlanetsAndGunsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class OFPLANETSANDGUNS_API AOfPlanetsAndGunsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
