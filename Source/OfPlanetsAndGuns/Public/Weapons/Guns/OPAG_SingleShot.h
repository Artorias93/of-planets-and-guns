#pragma once

#include "CoreMinimal.h"
#include "Weapons/Guns/OPAG_ModularGun.h"
#include "OPAG_SingleShot.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API AOPAG_SingleShot final : public AOPAG_ModularGun
{
	GENERATED_BODY()
	
public:
	AOPAG_SingleShot();
};
