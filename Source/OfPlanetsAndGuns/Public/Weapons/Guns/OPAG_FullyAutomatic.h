#pragma once

#include "CoreMinimal.h"
#include "Weapons/Guns/OPAG_ModularGun.h"
#include "OPAG_FullyAutomatic.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API AOPAG_FullyAutomatic final : public AOPAG_ModularGun
{
	GENERATED_BODY()
	
public:
	AOPAG_FullyAutomatic();
};
