#pragma once

#include "CoreMinimal.h"
#include "Weapons/Modules/OPAG_ModuleBase.h"
#include "OPAG_SightBase.generated.h"

class UCameraComponent;

UCLASS()
class OFPLANETSANDGUNS_API AOPAG_SightBase : public AOPAG_ModuleBase
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* SightCameraComponent;

public:
	AOPAG_SightBase();

protected:
	virtual void BeginPlay() override;
	virtual void OnAttach(const EModuleBrand NewBrand, const FOPAG_GunStats NewStats, const int
	                      NewEquippableFireTypes, const bool bIsAttachedToPlayer) override;

public:
	virtual void Tick(float DeltaTime) override;

public:
	FORCEINLINE UCameraComponent* GetSightCameraComponent() const { return SightCameraComponent; }

};
