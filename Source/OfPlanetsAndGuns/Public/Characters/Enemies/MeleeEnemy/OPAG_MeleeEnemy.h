#pragma once

#include "CoreMinimal.h"
#include "Characters/Enemies/OPAG_Enemy.h"
#include "OPAG_MeleeEnemy.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API AOPAG_MeleeEnemy : public AOPAG_Enemy
{
	GENERATED_BODY()	

	/////////////////////////////
	//Functions
	/////////////////////////////

public:

	AOPAG_MeleeEnemy();

	virtual void Attack() override;

	UFUNCTION(CallInEditor, Category = "OPAG AI")
	virtual void Death() override;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnAttackBegin(UAnimMontage* Montage);

	UFUNCTION()
	void OnAttackNotify(FName FunctionName, const FBranchingPointNotifyPayload& NotifyPayload);

	UFUNCTION()
	virtual void OnAttackEnd(UAnimMontage* Montage, bool bInterrupted);

	/////////////////////////////
	//Variables
	/////////////////////////////

protected: 

	UPROPERTY(EditDefaultsOnly, Category = "OPAG AI")
	UAnimMontage* MeleeAttackMontage;

	UPROPERTY()
	UAnimInstance* AnimInstance;

	UPROPERTY(EditDefaultsOnly, Category = "OPAG AI")
	float MeleeAttackMontagePlayRate = 1.9f;

	UPROPERTY(EditAnywhere, Category = "OPAG AI")
	FVector DamageCollisionBoxRelativeLocation = FVector(145.f, 0.f, 7.f);

	UPROPERTY(EditAnywhere, Category = "OPAG AI")
	FVector DamageCollisionBoxExtent = FVector(95.f, 20.f, 90.f);

	UPROPERTY(VisibleAnywhere)
	class UBoxComponent* DamageCollisionBox;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* SwordMesh;

	UPROPERTY(EditAnywhere)
	float MeleeWeaponDamage = 50.f;
};