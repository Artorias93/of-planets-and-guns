#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_ShieldAbilityCooldown.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_ShieldAbilityCooldown : public UGameplayEffect
{
	GENERATED_BODY()

public:
	UOPAG_ShieldAbilityCooldown();
};
