#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_HitPlayerShieldEffect.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_HitPlayerShieldEffect : public UGameplayEffect
{
	GENERATED_BODY()
	
public:
	UOPAG_HitPlayerShieldEffect();

};
