#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_HealthPotionCooldown.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_HealthPotionCooldown : public UGameplayEffect
{
	GENERATED_BODY()

public:
	UOPAG_HealthPotionCooldown();
};
