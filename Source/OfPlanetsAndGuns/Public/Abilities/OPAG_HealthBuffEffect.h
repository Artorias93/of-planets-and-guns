#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_HealthBuffEffect.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_HealthBuffEffect : public UGameplayEffect
{
	GENERATED_BODY()
	
public:
	UOPAG_HealthBuffEffect();

};
