// Copyright Epic Games, Inc. All Rights Reserved.

#include "OfPlanetsAndGuns.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OfPlanetsAndGuns, "OfPlanetsAndGuns" );
