#include "Weapons/Modules/SpecializedModules/Accessory/OPAG_AccessoryDrone.h"

AOPAG_AccessoryDrone::AOPAG_AccessoryDrone()
{
	PrimaryActorTick.bCanEverTick = true;

	AccessoryType = EAccessoryType::Drone;

}

void AOPAG_AccessoryDrone::BeginPlay()
{
	Super::BeginPlay();

}

void AOPAG_AccessoryDrone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}