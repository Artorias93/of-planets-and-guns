#include "Interactables/OPAG_LootBox.h"
#include "Weapons/Guns/OPAG_ModularGun.h"
#include "Items/OPAG_Drop.h"
#include "Managers/OPAG_GameInstance.h"
#include "Managers/OPAG_SoundManagerSubsystem.h"
#include <Components/PrimitiveComponent.h>

#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

#include "Materials/MaterialInstanceDynamic.h"

#include "Characters/Player/OPAG_Player.h"
#include "Interactables/OPAG_InteractableInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Weapons/Modules/OPAG_ModuleBase.h"


AOPAG_LootBox::AOPAG_LootBox()
{
	PrimaryActorTick.bCanEverTick = true;

	// Meshes
	Crate = CreateDefaultSubobject<UStaticMeshComponent>("Crate");
	if (Crate)
		RootComponent = Crate;

	Lid = CreateDefaultSubobject<UStaticMeshComponent>("Lid");
	if (Lid)
		Lid->SetupAttachment(RootComponent);
		
	// Particle system
	LootSpawnPoint = CreateDefaultSubobject<UStaticMeshComponent>("LootSpawnPoint");
	if (LootSpawnPoint)
		LootSpawnPoint->SetupAttachment(RootComponent);

	NcParticlesSign = CreateDefaultSubobject<UNiagaraComponent>("ParticlesFloatingSign_NC");
	if (NcParticlesSign) {
		NcParticlesSign->SetupAttachment(RootComponent);
		//NcParticlesSign->Activate(false);
	}
}

bool AOPAG_LootBox::WillChangeInteractStateDynamically_Implementation()
{
	return true;
}

FString AOPAG_LootBox::InteractString_Implementation()
{
	if (bHasBeenOpened)
		return "";
	else if(!bIsInteractable)
		return "Locked";
	else 
		return "Open Lootbox [E]";
}

void AOPAG_LootBox::BeginPlay()
{
	Super::BeginPlay();
	SMS = GetGameInstance<UOPAG_GameInstance>()->GetSoundManager();
		
	// Make lid material color pulse
	auto Material = Lid->GetMaterial(0);
	DynamicMaterial = UMaterialInstanceDynamic::Create(Material, NULL);
	Lid->SetMaterial(0, DynamicMaterial);
	DynamicMaterial->SetScalarParameterValue(TEXT("ActivatePulse"), 0.0f);

	// Start spawn particle system
	FVector CrateLocation = Crate->GetComponentLocation();
	if (NsActivationParticles != nullptr) {
		NcActivationParticles = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NsActivationParticles, CrateLocation,FRotator::ZeroRotator,FVector::OneVector,false,false);
	}

	// Niagara Sign
	if (NcParticlesSign != nullptr) {
		NcParticlesSign->Deactivate();
	}
}

void AOPAG_LootBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AOPAG_LootBox::SetItemsToSpawn(TArray<TSubclassOf<AActor>>& ItemsList)
{
	Items = ItemsList;
}

void AOPAG_LootBox::Unlock()
{
	bIsInteractable = true;
	
	if(SMS)
		SMS->PlayRewardSound(this);
	// Make Lid Material Pulse
	DynamicMaterial->SetScalarParameterValue(TEXT("ActivatePulse"), 1.0f);

	// Particle effects
	if (NsActivationParticles != nullptr) {
		NcActivationParticles->Activate();
	}
	if (NcParticlesSign != nullptr) {
		NcParticlesSign->Activate();
	}
}

void AOPAG_LootBox::SetModules(FOPAG_GunModules& ModulesStruct)
{
	Modules = ModulesStruct;
}

void AOPAG_LootBox::ActivationParticleEffect()
{
	if (NsActivationParticles != nullptr) {
		NcActivationParticles->Activate();
	}
	if (NcParticlesSign != nullptr) {
		NcParticlesSign->Deactivate();
	}
}

void AOPAG_LootBox::Interact_Implementation()
{
	// particle effect
	ActivationParticleEffect();
	// Explosion behavior with half parabolic method
	Lid->SetSimulatePhysics(true);
	FVector RandomDir = Lid->GetUpVector();
	float RandomAngle = FMath::DegreesToRadians(FMath::RandRange(0, 360));
	RandomDir.X = FMath::Sin(RandomAngle);
	RandomDir.Y = FMath::Cos(RandomAngle);

	const FVector Force = RandomDir * ThrowLid;
	Lid->AddForce(Force, NAME_None, true);
	Lid->AddTorqueInRadians(RandomDir * 100, NAME_None, true);
	// Set can interact to false after player interact with box
	bIsInteractable = false;

	bHasBeenOpened = true;


	if (SMS) {
		SMS->StopRewardSound(); 
		SMS->PlayEnviromentSounds(this, EEnviromentSoundAction::Chest_Open);
	}

	// Active Timer for spawn items
	SpawnItem();

	// Make Lid Material Stop Pulse
	DynamicMaterial->SetScalarParameterValue(TEXT("ActivatePulse"), 0.0f);
}

bool AOPAG_LootBox::CanInteract_Implementation()
{
	return bIsInteractable;
}

// Call method for any item in the list
void AOPAG_LootBox::SpawnItem()
{
	if (Items.Num() == 0)
	{
		return;
	}

	// Spawn Item
	const FVector SpawnLocation = LootSpawnPoint->GetComponentLocation();//RootComponent->GetComponentLocation();
	const FRotator SpawnRotator = FRotator::ZeroRotator;
	AActor* Item = GetWorld()->SpawnActor(Items[0], &SpawnLocation, &SpawnRotator);
	Items.RemoveAt(0);

	if (Item)
		SpawnedItem = Item;

	// If the item is gun, that gun is generate with random modules
	if (AOPAG_ModularGun* GunModular = Cast<AOPAG_ModularGun>(Item))
	{
		GunModular->CreateAndAttachAllModules(Modules, GunModular->GetSecondaryStatsModifiers());
		GunModular->StartDropTimeline();
	}
	// If the item is a module, apply bonus stats
	if (AOPAG_ModuleBase* Module = Cast<AOPAG_ModuleBase>(Item))
	{
		Module->StartDropTimeline();

		const AOPAG_Player* Player = Cast<AOPAG_Player>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (Player != nullptr)
		{
			const AOPAG_ModularGun* PrimaryGun = Player->GetPrimaryGun();
			if (PrimaryGun != nullptr)
			{
				Module->SetStats(PrimaryGun->GetSecondaryStatsModifiers());
			}
		}
	}

	if (AOPAG_Drop* Drop = Cast<AOPAG_Drop>(Item))
	{
		Drop->StartDropTimeline();
	}

	// Re-call this method for any items
	GetWorld()->GetTimerManager().SetTimer(SpawnItemsHandle, this, &AOPAG_LootBox::SpawnItem, 0.5, false);
}